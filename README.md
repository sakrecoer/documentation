# Devuan Project Documentation

## Devuan specific documentation

 * [devuan art](./art/)
 * [devuan maintainers docs](./maintainers/)
 * [Manuals](./manuals/)
 * [release-docs](./release-docs/)

## Archived docs

 * [devuan project docs](./archive/project/)
 * [devuan news docs](./archive/news/)
 * [dev1fanboy's wiki and pages](./archive/dev1fanboy/)
 
## Contributing

In order to contribute to this documentation, simply fork the project and create a Merge Request.
After having seen your contributions for a while, you may request developer access, which will allow you to contribute on this repository directly.
