# Unstable: Ceres

This release has a fixed codename.  For compatibility with debian terminology, it is also aliased as `sid`.

## The Planet

[(1) Ceres](http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=1) was the first recorded discovery of a minor planet, on January 1st, 1801, by Giuseppe Piazzi, from the Observatory of Palermo.  It is a dwarf planet, and the largest member of the asteroid belt.  Soon after his discovery, Piazzi wrote: "I have announced this star as a comet, but since it is not accompanied by any nebulosity and, further, since its movement is so slow and rather uniform, it has occurred to me several times that it might be something better than a comet. But I have been careful not to advance this supposition to the public."  He lost Ceres in the glare of the Sun after one month of tracking, but could not find it again because of the lack of proper method to predict its orbit.  The young Carl Friedrich Gauss, at the time 24, solved this problem in a few months, inventing [a new method for preliminary orbit determination](https://en.wikipedia.org/wiki/Gauss%27_Method) still in use today.

[![PIA17651_modest](https://git.devuan.org/uploads/devuan/devuan-project/81d8ee6de5/PIA17651_modest.jpg)](http://www.nasa.gov/mission_pages/dawn/main/)

[releases](../releases) - stable: [jessie](jessie) - testing: [ascii](ascii) - unstable: [ceres](ceres)

