# Devuan News Issue LVII

__Volume 03, Week 1, Devuan Week 57__

Released 12016/01/04

https://git.devuan.org/devuan-editors/devuan-news/wikis/past-issues/volume-03/issue-057

## Editorial

Happy new year from the Devuan news team!

In case you wondered, yes Devuan is alive and kicking. Devuan Weekly News hasn't released a single issue since last June.
We're very sorry about that. Did you miss it? [Tell us!][feedback]

Last year there was a lot of community effort put into making Devuan and the user experience better, with much cooperation between members of the mailing list. This includes new software for use in Devuan, documentation, and the start of packaging for vdev.

Aside from the growing strength of the community, we have seen significant progress towards init freedom in Devuan and the approaching beta release. Important init freedom issues have been solved, and security issues will soon come into focus with an eye on the beta release for critical security updates. We are calling for volunteers on this, so feel free to discuss this on the mailing list.

This year will be the year of init freedom, cooperation and community. We hope you will join us in celebrating freedom and justice!

-- @dev1fanboy

## Comings and Goings

The end of the year 12015 was marked by the unfortunate [death of Debian founder Ian Murdock][IM].

### Lately in Devuan

### [A discussion about mount-points][1]

Steve Litt asked about the preferred behaviour for an auto-mounter program he is writing in relation to his Python presentation at GoLUG, and the discussion that followed turns out to be instructive for managing mount points. Teodoro Santoni's comments provide [useful tips about UUID's][2], ~~Arnt Karlson~~ Stephanie Daugherty talked about the [purpose of the /media directory][3] and Adam Borowski expanded on this by explaining the [security implications behind the mount point structures][4].

### [APT pinning compatibility is broken][5]

Mitt Green mentioned that upstream changes to APT have landed in the Ceres branch of Devuan, resulting in a break of compatibility for APT pinning rules. Jaromil commented that Devuan [does not rely on pinning][6] to protect against the systemd avalanche. Franco Lanza explained that [pinning can be done on the "server side"][7] and the pinning included in the install [devuan-baseconf] is scheduled for removal for the beta.

### [Init freedom is complete in the base install][8]

Daniel Reurich recently fixed an issue that would result in systemd-sysv being the default init at install-time in some cases. Whilst problems concerning init freedom are [not yet solved completely][9], this solves the problem of systemd-sysv being required by default for some debootstrap installs and images built using vmdebootstrap.


### [Security advisory discussions] [10]

Back in April hellekin [announced the Devuan talk forum][11] which uses discourse to combine documentation with social features and a platform for discussion. Devuan talk users recently received an email notifying them that security advisories may be discussed there once they begin to be published. Shortly after this notification hellekin [proposed a devuan-security group for the gitlab][32] and is calling for volunteers for the security team.

### [Upower is now working and init agnostic][12]

Earlier this month Per Eric Rosén asked how it was possible to [use automatic suspend for the mate desktop on Jessie][13]. Adam Borowski replied saying that a [fork of upower is needed because it depends on systemd][14] for power management features. Daniel Reurich (Centurion_Dan) recently fixed this issue and announced on the Devuan development channel that upower is now working correctly for i386 and amd64, and that it has the necessary pm-utils support. This change means that the xfce4 and mate desktops both have power management support that doesn't rely on systemd, and mate is close to being free of systemd dependencies altogether.

### [Vdev packaging is underway] [15]

Last month aitor_czr mentioned that he has started packaging [vdev][16], the Devuan supported device manager. Once the packaging process is complete vdev will then be able to enter the experimental branch to be tested in Devuan.

### [Working with recommends and suggests][17]

In a post started by Emiliano Marini about [nmap dependencies][18] fsmithred provided some useful information for working with dependencies, suggests and recommends. Simon Wise built upon this by providing some [information about APT preferences][19] and [referred to the apt-cache manual][20] for further reading.

## New projects for Devuan

### [Devuan migration and minimalism wiki] [21]

Dev1fanboy posted a [quick start guide to Devuan migration][22], showing how to migrate to Devuan and configure the install to be more lean. He has since started a wiki project which has received a number of contributions from others, including a [German translation][23] from a contributor who wished to remain nameless, an [Italian translation][24] from Antonio Trkdz, and a [Spanish translation][25] from Emiliano Marini which was [verified by aitor_czr][26] who also provided corrections.

### [Netman] [27]

Netman is a network manager written by Edward Bartolo with the aim to be portable, use minimal dependencies and adhere to the KISS philosophy ("Keep It Simple Stupid"). The project was uploaded to the Devuan gitlab where it has received contributions from others including [art work][28] from Rich White, [code contributions][29] from tilt! and a [gtk3 frontend][30] started by aitor_czr. Recently there was a [successful git build][31] for the netman packages meaning that it can soon enter the experimental branch to be tested for usability in Devuan.

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Read you next week!

Devuan News is made by your peers: you're [welcome to contribute][wiki]!

* @hellekin (editor at large)
* @golinux (den mother)
* @CenturionDan (Devuan's packaging master)
* @dev1fanboy (essayist)

[IM]: https://bits.debian.org/2015/12/mourning-ian-murdock.html "Debian mourns the death of founder Ian Murdock"
[1]: https://lists.dyne.org/lurker/message/20151225.174135.dd74c0ab.en.html "Steve Litt asks about prefered auto mounter behaviour"
[2]: https://lists.dyne.org/lurker/message/20151225.235048.e944f0dd.en.html "Teodoro Santoni talked about labels and UUID's"
[3]: https://lists.dyne.org/lurker/message/20151225.213258.2828fd28.en.html "Stephanie Daugherty talked about the purpose of the /media directory"
[4]: https://lists.dyne.org/lurker/message/20151226.054012.e204f2e8.en.html "Adam Borowski explained the security implications of /media and /mnt"
[5]: https://lists.dyne.org/lurker/message/20151202.084849.315c8388.en.html "APT pinning breaks compatibility"
[6]: https://lists.dyne.org/lurker/message/20151202.101753.fbed57f0.en.html "Jaromil commented on APT pinning"
[7]: https://lists.dyne.org/lurker/message/20151202.110859.7749ac9c.en.html "Pinning can be done on the server side"
[8]: https://git.devuan.org/d-i/base-installer/issues/26 "Daniel Reurich fixed an issue that resulted in systemd-sysv being the default init"
[9]: https://git.devuan.org/devuan/devuan-maintainers/issues/17 "not all problems concerning init freedom are solved yet"
[10]: https://talk.devuan.org/t/security-advisories/173 "Devuan talk security advisory discussion"
[11]: https://lists.dyne.org/lurker/message/20150402.235315.c2edb6f9.en.html "Devuan talk announced"
[12]: https://botbot.me/freenode/devuan/search/?q=upower+is+fixed+and+in+merged+now "Daniel Reurich announced a fix to upower"
[13]: https://lists.dyne.org/lurker/message/20151201.152505.689a7c0c.en.html "upower in Devuan questions by Per Eric Rosén"
[14]: https://lists.dyne.org/lurker/message/20151201.195000.f8ccdde7.en.html "response from Adam Borowski regarding upower"
[15]: https://lists.dyne.org/lurker/message/20151107.210914.63365d83.en.html "aitor_czr is packaging vdev"
[16]: https://github.com/jcnelson/vdev "the vdev project"
[17]: https://lists.dyne.org/lurker/message/20151222.151445.2f45ca28.en.html "fsmithred's tips about working with dependencies and recommends"
[18]: https://lists.dyne.org/lurker/message/20151222.115427.8b6f7e58.en.html "Nmap dependency problems"
[19]: https://lists.dyne.org/lurker/message/20151223.081726.d94006e3.en.html "apt preferences tips by Simon Wise"
[20]: https://lists.dyne.org/lurker/message/20151223.222900.a3276be4.en.html "apt-cache manual referred to by Simon Wise"
[21]: https://git.devuan.org/dev1fanboy/Upgrade-Install-Devuan/wikis/home "Devuan GNU/Linux Installation, upgrades and minimalism wiki"
[22]: https://lists.dyne.org/lurker/message/20151103.205147.1123933f.en.html "Quick start guide to upgrading to Devuan and configuring minimalism"
[23]: https://git.devuan.org/dev1fanboy/Upgrade-Install-Devuan/wikis/German-Translation "German translation of quick start guide by a nameless contributor"
[24]: https://lists.dyne.org/lurker/message/20151216.125624.7084a65f.en.html "Italian quick start guide translation"
[25]: https://lists.dyne.org/lurker/message/20151216.153044.8e9ad6de.en.html "Spanish translation by Emiliano Marini"
[26]: https://lists.dyne.org/lurker/message/20151217.114752.6602b0ca.en.html "Spanish corrections by Aitor"
[27]: https://git.devuan.org/edbarx/netman "Edward Bartolo's netman project"
[28]: https://lists.dyne.org/lurker/message/20151020.184848.0c0ca7eb.en.html "netman art work contribution by Rich White"
[29]: https://lists.dyne.org/lurker/message/20150825.110107.79d605f4.en.html "a code contribution to netman by tilt!"
[30]: https://lists.dyne.org/lurker/message/20151115.202159.91a9507b.en.html "aitor_czr's gtk3 front-end"
[31]: https://lists.dyne.org/lurker/message/20151223.181438.5c974c84.en.html "netman builds on gitlab"
[32]: https://lists.dyne.org/lurker/message/20151224.163605.341196fe.en.html "hellekin is calling for volunteers for the security team"

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/devuan-editors/devuan-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/devuan-editors/devuan-news/wikis/home "Devuan News"
[feedback]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Let us know what you think"
