# Devuan Weekly News Issue XIII

__Volume 002, Week 8, Devuan Week 13__

Released 24/02/12015 [HE](Why-HE)

https://git.devuan.org/Envite/devuan-weekly-news/wikis/past-issues/volume-002/issue-013


## Editorial

In the same way that Devuan is the project of a group, with people
more dedicated and people around giving advice and support, and
thoughts and opinions, the Devuan Weekly News (soon to change name)
is the project of a group as well. Like Devuan, it is a growing group,
with a small core doing the hard resuming work but several other eyes
providing advice, hunting for typos, making small text contibutions
and, in general, proofreading.

I want to expressly thank those people who make the last issues of
DWN possible. You can [contribute too][wiki]!

.-Envite


## Last Week in Devuan

### Unanswered questions

Some threads on the list are questions that have been not answered.

Isaac Dunham [asks][1] for the best way to split a library package,
for his libsysdev project.

### [Trios packages openrc][2]

lkcl reports that somebody at Debian User Forums reports that Trios
GNU/Linux uses OpenRC and is free of systemd, and wonders about this
kind of info being hard to find. golinux answers that this was already
posted on this list. Some other list members informed that it works
for them.

### [Adoption of packages][3]

In the long thread about *towards systemd-free packages* hellekin
reminds us that

> Volunteers for package adoption can now head to the
> [devuan-maintainers][0] project.

### [The Onion Principle][4]

Noel shows us why concentrating in developing only core systemd-free
packages does not mean that we are currently a real fork nor a
derivative, since that depends on what we add to that core in
subsequent releases.

> To resume the principle: The best way to create a very complex
> project is to add one layer at a time.

### [XFCE et al][5]

David H points us at XFCE which runs in FreeBSD and OpenIndiana
virtual Machines as well as in Linux, and is about to publish a new
release. He wonders if one of the Devuan developers should contact
them, and the unanimous answer was that he can do that himself.

On a [later thread][6], David confirmed that he contacted them by
e-mail.

### [removing systemd and libsystemd0 in a desktop][7]

lkcl reports that he progressed in the task of removing libsystemd0
in a computer running a desktop. He later reports that turning off
autoconfiguration on Xorg makes the trick.

Isaac Dunham reports that he has been able too to have a working Xorg
without udev, thanks to a clever trick:

> The trick is that input devices have a description at
> /sys/dev/char/<major>:<minor>/device/name

Now it seems clear that a good part of the Depends on udev are not
true dependencies.

lkcl also [publicited it in slashdot][8].

### [mdev packaging][9]

In a branch to the previous thread, Isaac Dunham reports that he has
packaged mdev. Then it follows some back and forth about issues with
the new code. This is an architecture-independent script to make boxes
bootable without udev.

### [mdev and udev][10]

In a sub-branch of the previous branch, Godefridus Daalmans asks if
purging udev means creates need for the old-style makedev. Isaac
Dunham answers that mdev does not depend on makedev.

### [LoginKit on the pre-alpha][11]

Dima reports that the Valentine's pre-alpha is able to work with
LoginKit.

### [systemd free badge][12]

Jaromil suggests the idea of creating a badge about a distribution
being systemd-free, to be used by all distributions that share the
same idea.

hellekin raises the concern that it might be overpolarizing the issue
and maybe giving systemd some publicity it doesn't deserve.

Joel Roth, on the other hand, suggested using a different text for the
badge on the lines of Classical Unix Administration and some others.

David Harrison suggests changing to the stanza of an [Init Freedom
badge][13]. Init Freedom gained some traction on the list.

### [Linux kernel and the force behind it][14]

hal stars a discussion about an [Ars Technica article][15] about the
development speed of the Linux kernel, and how it may be being
directed by big companie like Red Hat Inc.

Gravis makes a point about most patches coming from companies are for
drivers, bug fixes and new features, not for changinf the main
direction of the kernel development.

John Crisp cites a [comment from Trevor Potts][16] to an article in
The Register, the comment indicating that the point on systemd is that
Red Hat is effectively trying to dominate the Linux ecosystem by
making as much software as possible dependent on systemd and thus
render Linux (and Linus) prescindible.

Discussion then mutated in another one about intended audience for
Devuan, and later on to a one about pros and cons of dbus over
alternative solutions like standard IPC, SunRPC, and ZeroMQ.

A branch on the audience side started by Nate praises the pragmatic
approach of going with Xfce as Devuan's default desktop for the first
release.

### [KDE systemd lock-in][17]

golinux reports that [KDE Will Depend on 'logind' and 'timedated' in 6
Months][18]. The original article specifies that it will not depend on
systemd as init system.

### [UEFI Secure Boot not secure][19]

Robert Storey expresses concerns about UEFI being even more vulnerable
than BIOS to malware, and BIOS motherboards becoming scarce. Gravis
sums up reminding issues detected on hard disks firmware as well. It
may be that the future is in writing free formware.

### [Easy forkability][20]

In a cryptically named thread, Ста Деюс asks how to protect Devuan if
the kind of people who corrupted Debian comes and infects it. Hendrik
Boom (changing the thread Subject to something more akin) answers that
a priority for Devuan should be making it easy to fork, for if it
becomes needed.

Katolaz also reminds us that freedom of choice is a mere illusion on
most sides of any distro: kernel, libc, pam, X, etc. While the
principle is sound, its feasibility is not guaranteed.

Martijn Dekkers points out the "benevolent dictator" way the VUAs use
on Devuan may be the best.

### [Looking for advice in preparation to switch from Debian to Devuan][21]

Anto asks how to smoothly upgrade from Debian Wheezy or Jessie to
systemd. Nuno Magalhães suggests to pin on wheezy versions, and
reading the documentation that this list has produced.

### [GDM switched to Wayland by default][22]

Jaromil reports that GDM will use Wayland by default. Concerns were
raised about the degree of dependency of Wayland on systemd, but it
seems that the Wayland protocol is independent on systemd, according
to Jude Nelson and Jack L. Frost.

### [systemd NOCOW on btrfs][23]

Jim Murphy wonders about systemd putting its journal file with the
NOCOW flag. Didier Kryn and Adam Borowski elaborates on how it exactly
works.

Some off-branch discussion arised about kludging own code around
compiler (or even library) issues.

### [Three usability features][24]

Jonathan Wilkes points out how important (and safe) it could be for
novice users to have

* A selectable list of wifi connections

* An immediate search for applications by natural language typing or
  name

* A menu raising after pressing the Super (Windows logo) key

Ron points out that 1 can be done with wicd. Jaromil states that these
issues are important but not a priority for Devuan Jessie. Steve Litt
gives dmenu and umenu as examples of programs able to cope with 2 and
3.

### [vdev status update][25]

Jude gives an update on the status of vdev. It is

> closer to being done than closer to having been just started, and
> it's mature enough that early testers can start experimenting with
> using it

Some technical interchange comes later to enrich mdev and udev.

### [plan to install valentine pre-alpha on real hardware][26]

Hendrik Boom reports he is about to install Valentine's pre-alpha on
real hardware, and requests advice about how to make it boot from an
USB drive. Jude explains about dd and Envite about BIOS boot method.


## In the press

### Linux Format February 2015 (Issue 194)

The Linux press is taking notice of the Devuan project. Issue 194 of
Linux Format carries a short article outlining the Debian schism and
Devuan's manifesto as outlined on the website.

The article quotes the devuan site several times and includes a link.
The subsequent editorial remarks are fairly balanced, tough a little
downbeat about Devuan's chances to win users over from Debian. Some
here might, of course, disagree.

See [this Tuxradar page](http://tuxradar.com/content/linux-format-194-next-gen-linux)
for details of issue 194 (February 2015).


## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Help us to [write the issue for the next week][wiki]. There will be a lot to read!

Thanks for reading us this week.

Sincerely yours, the Sleepless Team:
* Noel, "er Envite" (Editor)
* hellekin (Markup master)
* golinux (main typo spotter)
* David Harrison (press clipper)
* DocScrutinizer05 (fellow typo spotter)


[0]: https://git.devuan.org/devuan/devuan-maintainers#tab-readme "Volunteers for package adoption"
[1]: https://lists.dyne.org/lurker/message/20150217.233504.3df6e5b3.en.html "Question about properly splitting packages"
[2]: https://lists.dyne.org/lurker/message/20150217.174901.c141a0f6.en.html "trios already packaging openrc and removed libsystemd0"
[3]: https://lists.dyne.org/lurker/message/20150219.010846.777b69f8.en.html "Volunteers for package adoption"
[4]: https://lists.dyne.org/lurker/thread/20150219.170948.cb8b18c8.en.html "The Onion Principle"
[5]: https://lists.dyne.org/lurker/thread/20150220.214945.2de71a8e.en.html "XFCE et al"
[6]: https://lists.dyne.org/lurker/message/20150221.130231.b11e3dba.en.html "Xfce"
[7]: https://lists.dyne.org/lurker/message/20150215.162755.79dd4285.en.html "successfully manually removing systemd and libsystemd0 from debian and still maintaining a working desktop"
[8]: http://slashdot.org/submission/4203115/removing-libsystemd0-from-a-live-running-debian-system "removing-libsystemd0-from-a-live-running-debian-system"
[9]: https://lists.dyne.org/lurker/message/20150220.155742.453cdaaa.en.html "WIP: mdev packaging"
[10]: https://lists.dyne.org/lurker/message/20150220.200856.45450e4f.en.html "about mdev and udev"
[11]: https://lists.dyne.org/lurker/message/20150220.214457.fe7abd6e.en.html "LoginKit on the pre-alpha"
[12]: https://lists.dyne.org/lurker/message/20150221.090804.12f3c0d3.en.html "systemd free badge"
[13]: https://lists.dyne.org/lurker/message/20150221.121457.86d0308b.en.html "Init freedom badges"
[14]: https://lists.dyne.org/lurker/message/20150219.123628.fdfef01e.en.html "OT: Linux kernel and the force behind it"
[15]: http://arstechnica.com/information-technology/2015/02/linux-has-2000-new-developers-and-gets-10000-patches-for-each-version/ "Linux has 2,000 new developers and gets 10,000 patches for each version"
[16]: http://m.forums.theregister.co.uk/post/reply/2375127 "Re: If systemd is so bad..."
[17]: https://lists.dyne.org/lurker/message/20150220.165933.cfc6af96.en.html "KDE systemd lock-in"
[18]: https://soylentnews.org/article.pl?sid=15/02/20/101235 "KDE Will Depend on 'logind' and 'timedated' in 6 Months"
[19]: https://lists.dyne.org/lurker/message/20150222.160654.74bec919.en.html "UEFI secure boot not secure"
[20]: https://lists.dyne.org/lurker/message/20150222.143851.c07b6a9f.en.html "Dng Digest, Vol 5, Issue 11"
[21]: https://lists.dyne.org/lurker/message/20150216.201613.e9157e21.en.html "Looking for advices in preparation to switch from Debian to Devuan"
[22]: https://lists.dyne.org/lurker/message/20150222.102759.b69f8838.en.html "GDM switched to wayland by default"
[23]: https://lists.dyne.org/lurker/message/20150222.182806.cfcab7f6.en.html "OT - It may be only one file, but it does point to the bigger problem!"
[24]: https://lists.dyne.org/lurker/message/20150222.201112.1e04eb50.en.html "three important UI features"
[25]: https://lists.dyne.org/lurker/message/20150223.010058.a39e855f.en.html "vdev status update"
[26]: https://lists.dyne.org/lurker/message/20150223.212412.c6bb71b4.en.html "plan to install valentine pre-alpha on real hardware."

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
