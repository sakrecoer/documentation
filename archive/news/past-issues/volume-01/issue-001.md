Author: Noel Torres

Date: 2014-12-02 00:14 +000

To: dng

Subject: [Dng] Devuan Weekly News I


Ok, so that I'm not useful by packing, yet, I'll try to be useful in other 
ways. 

So welcome to Devuan Weekly News. Weekly issue I also corresponds to Week 1 of 
the project. 


* Project name: 

There has been some discussion about the name 'Devuan' being the best (e.g. 
[1]). No conclusive answer has been given in favor or against. 

[1] https://lists.dyne.org/lurker/message/20141130.225407.850e868d.en.html 


* Fraud accusation and requests for personal and financial data: 

There has been a very long and harsh thread starting at [2] with one side 
requesting data about the people who runs the project and is asking for 
donations, and the other side explaining that some information will arrive on 
its own time, since the project has just started ([3]), that more info is 
needed but the ways of requesting were unappropiate ([4]), that the 
information is in fact available ([5]), and finally some information was 
clearly expressed at [6]. 

[2] https://lists.dyne.org/lurker/message/20141201.170833.e3b54777.en.html 

[3] https://lists.dyne.org/lurker/message/20141201.174747.403042cb.en.html 

[4] https://lists.dyne.org/lurker/message/20141201.204940.50374e44.en.html 

[5] https://lists.dyne.org/lurker/message/20141201.182743.c5b53e30.en.html 

[6] https://lists.dyne.org/lurker/message/20141201.210624.0e629859.en.html 


* Project Logo: 

There has been some work around logos for the project in the thread at [7] 
with proposals around a mouse, the Huffington Horse, a wizards version of the 
Debian logo and a wavy spiral, in several colours, mainly orange and black. No 
conclusion has been achieved, yet. 

[7] https://lists.dyne.org/lurker/message/20141130.211706.71a2b821.en.html 


* Inclusion of systemd 

Another hard thread, but not so harsh as the Fraud one, was started at [8] 
with a statament that systemd should be supported in Devuan as an option to 
users, since users' freedom to choose an init system should include systemd. 
Clear stataments were done about the issue at [9] saying that systemd will be 
supported, and later clarified at [10] saying that Devuan is not to 'Put in 
effort to keep systemd out because we hate it' but 'If systemd upstream or any 
interested party puts in effort to make systemd fit in with the bazaar approach, 
then we should welcome them with open arms'. 

[8] https://lists.dyne.org/lurker/message/20141130.125423.9da1f8c2.en.html 

[9] https://lists.dyne.org/lurker/message/20141130.131414.34f133f0.en.html 

[10] https://lists.dyne.org/lurker/message/20141201.080352.494a6c5c.en.html 


* Udev 

Thre was also a discussion about if Devuan should ship udev at all, starting 
at [11], with proposals for eudev and plain devfs ([12]). 

[11] https://lists.dyne.org/lurker/message/20141129.223725.260e1585.en.html 

[12] https://lists.dyne.org/lurker/message/20141130.165311.0d85d31b.en.html 


* 4chan 

There was a short thread about how Devuan has been received at 4chan, 
specifically at /g/, at [13]. 

[13] https://lists.dyne.org/lurker/message/20141201.144055.935d96ac.en.html 


* How to work on Devuan 

There was a request at [14] asking for information about how to really 
contribute. No information about how to code was provided, but the priorities 
were clearly pointed at that we need an installer ([15]). 

[14] https://lists.dyne.org/lurker/message/20141130.164003.44f07a0b.en.html 

[15] https://lists.dyne.org/lurker/message/20141130.175322.3092e153.en.html 


Ok, that's all for today. Let's hope next week we all have less harsh to read 
(and less to read in general) and more to work on. 

Noel 
er Envite 