# Crittaggio totale del disco

Questa è una semplice guida passo-passo per implementare il crittaggio di tutto il disco nel corso di un'installazione di Devuan. È richiesto il completamento di tutte le fasi della procedura d'installazione fino al raggiungimento del menù di partizionamento dei dischi (Le immagini sono relative alla procedura d'installazione in inglese).

## Partizionamento dei dischi

**1) Scegliete di usare tutto il disco e di inizializzare LVM crittato. Questa è la soluzione consigliata  
in quanto più facile per i novizi.**

![Partizione con LVM e crittaggio](../img/crypt1.png)

&nbsp;

**2) Scegliete il disco da crittare. Nell'esempio ce n'è solo uno.**

![Selezione di un disco da crittare](../img/crypt2.png)

&nbsp;

**3) Come al solito, viene richiesto di scegliere uno schema di partizionamento. La soluzione più facile  
consiste nell'installare tutto su un'unica partizione.**

![Selezione di uno schema di partizionamento](../img/10part2.png)

&nbsp;

**4) Dovrete scrivere lo schema di partizionamento sul disco per poter iniziare il crittaggio.**

![Scrivi su disco](../img/crypt3.png)

&nbsp;

## Crittaggio del disco

**5) L'installatore vi informerà che il disco verrà cancellato. L'operazione richiederà del tempo se  
il disco ha molto spazio disponibile.**

![Cancellazione del disco](../img/crypt4.png)

&nbsp;

**6) Ora bisogna creare una chiave crittografica. Per una chiave sicura si raccomandano almeno  
20 caratteri, composti da lettere maiuscole e minuscole, almeno tre numeri e almeno due simboli.**

![Immissione di una chiave crittografica](../img/crypt5.png)

&nbsp;

**7) Viene richiesto di impostare la dimensione del gruppo di volumi. La soluzione più semplice  
consiste nell'accettare il default, che userà l'intero disco.**

![Selezione della dimensione del volume](../img/crypt6.png)

&nbsp;

**8) Esaminate il riassunto delle modifiche da scrivere e, se vi soddisfano, confermate la scrittura.**

![Modifiche](../img/crypt7.png)

&nbsp;

**9) Finalmente, confermate le modifiche e continuate [la procedura di installazione](devuan-install.md).**

![Conferma le modifiche](../img/crypt8.png)

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
