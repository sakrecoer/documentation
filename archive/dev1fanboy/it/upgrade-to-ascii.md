Questa guida descrive come aggiornare Devuan Jessie a Devuan ASCII. Da per scontato che si parta da un'installazione funzionante di Devuan Jessie e non dovrebbe essere usata per le migrazioni da altri sistemi.

# Aggiornamento da Devuan Jessie ad ASCII
Per prima cosa modifcate il file sources.list in modo da sostituire la versione con ASCII.

`root@devuan:~# editor /etc/apt/sources.list`

Aggiungete i mirror Devuan più aggiornati e ASCII come versione. Commentate tutte le altre linee.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Aggiornate il "portachiavi" Devuan per essere sicuri di avere installata l'ultima versione disponibile.

`root@devuan:~# apt-get upgrade devuan-keyring`

Aggiornate l'indice dei pacchetti.

`root@devuan:~# apt-get update`

Non vi resta che aggiornare il sistema.

`root@devuan:~# apt-get dist-upgrade`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
