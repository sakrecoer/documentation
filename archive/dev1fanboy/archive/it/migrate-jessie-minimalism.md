# Migrazione minimalista verso Devuan Jessie

Questa guida vi mostrerà come migrare verso Devuan, come configurare apt per minimizzare il numero dei pacchetti installati, come rimuovere D-Bus dal sistema e l'amministrazione di rete relativa.

## Migrazione
Per iniziare aprite il file sources.list in modo da aggiornare l'indice dei pachetti con apt-get.

`root@debian:~# editor /etc/apt/sources.list`

Commentate tutte le altre linee presenti nel file sources.list e aggiungete le seguenti linee.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Aggiorniamo l'indice dei pacchetti così da poterli scaricare dai repository Devuan.

`root@debian:~# apt-get update`

Prima di tutto dobbiamo installare il "portachiavi" Devuan e poi aggiornare di nuovo la lista dei pachetti in modo da poterli autenticare.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Una volta installato il "portachiavi" Devuan dovreste aggiornare di nuovo l'indice, ricevendo d'ora in poi solo pacchetti autenticati.

`root@debian:~# apt-get update`

Finalizziamo il processo di aggiornamento.

`root@debian:~# apt-get dist-upgrade`

## Configurazione minimalista
Grazie alla dritta di [TheFlash] vi sarà possibile sgrassare il vostro sistema in modo semplice e pulito. Configureremo apt in modo da non considerare come importanti i pacchetti raccomandati.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Aggiungete le seguenti linee:

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

Sebbene molti pacchetti raccomandati non siano veramente importanti, ce ne sono alcuni che vanno protetti dalla rimozione.

Per la sicurezza sia dei browser che delle altre applicazioni, è bene assicurarsi che i certificati SSL siano sempre disponibili, per questo installate il pacchetto ca-certificates. Saltate questo passaggio solo se siete ben consapevoli delle conseguenze.

`root@devuan:~# apt-get install ca-certificates`

Il pacchetto  dei certificati SSL verrà contrassegnato come installato manualmente, anzichè come dipendenza o raccomandato. Qualora il prossimo passaggio mostri dei pacchetti che desideriate mantenere potrete contrassegnarli allo stesso modo prima della conferma di qualsiasi rimozione.

`root@devuan:~# apt-get autoremove --purge`

## Devuan senza D-Bus
La rimozione di dbus è più complicata e il compromesso è d'obbligo.

### Montare i volumi come utente
Un'alternativa all'auto-mounting D-bus dipendente consiste nel preparare da voi i punti di mount e nell'installare un gestore dei file in grado di montare i volumi senza utilizzare D-Bus.

Andremo a modificare fstab quindi è meglio farne prima una copia.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Ora potete modificare il vostro fstab.

`root@devuan:~# editor /etc/fstab`

Aggiungete la seguente linea al vostro fstab, adattandola se necessario ai parametri corretti per i vostri dischi USB. Assicuratevi di aver selezionato l'opzione `user` in modo da permettere agli utenti non-root di montare il disco.

~~~
/dev/sdb1        /media/usb0    auto    user,noauto    0 0
~~~

Per determinare i parametri del disco corretti potete usare il comando `lsblk`. 

Creiamo il punto di mount per i nostri dischi USB.

`root@devuan:~# mkdir /media/usb0`

Inserite un disco USB e verificate che la vostra configurazione funzioni correttamente.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### Installare software non dipendente da D-Bus
Quasi tutti gli ambienti grafici completi richiedono dbus, quindi si dovrebbe optare per una semplice interfaccia grafica. Qui useremo fluxbox in quanto facile da usare, veloce e facilmente scalabile.

`root@devuan:~# apt-get install fluxbox menu fbpager feh`

Usate il comando update-alternatives per selezionare startfluxbox come interfaccia grafica di default.

`root@devuan:~# update-alternatives --config x-window-manager`

Se volete includere anche un gestore grafico di sessione potete usare WDM.

`root@devuan:~# apt-get install wdm`

Altrimenti lo script startx può essere lanciato dal terminale dopo il log in come utente.

`user@devuan:~$ startx`

Potete usare xfe come gestore file in grado di montare dischi rimovibili senza utilizzare auto-mounter.

`root@devuan:~# apt-get install xfe`

Firefox-esr rappresenta una buona scelta per un browser internet che non dipenda direttamente da D-bus. 

`root@devuan:~# apt-get install firefox-esr`

### Configurazione della rete
Invece di usare un gestore di rete dipendente da D-bus configureremo manualmente la rete per l'utilizzo di interfacce multiple.

`root@devuan:~# editor /etc/network/interfaces`

Ecco una configurazione per reti senza fili multiple sulla stessa interfaccia. Aggiungendo un'istanza di rete usata saltuariamente, potrete cambiare la configurazione di default al bisogno. Per maggiori informazioni consultate [il manuale debian](https://www.debian.org/doc/manuals/debian-reference/ch05.it.html#_the_manually_switchable_network_configuration) riguardo alla configurazione di rete commutabile alla quale mi sono riferito.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase

iface work inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

Così per esempio potete impostare la rete "work" per mezzo della sequenza di comandi `ifdown wlan0` e `ifup wlan0=work` come root.

La configurazione per le reti cablate è molto più semplice.

~~~
# Configuarazione di rete automatica, inizializzata solo in caso di conessione rilevata.
allow-hotplug eth0
iface eth0 inet dhcp

# Configurazione di rete statica, inizializzata al boot.
auto eth1
iface eth1 inet static
        address 192.168.1.5
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

Per maggiori informazioni sulle interfacce di rete consultate `man 5 interfaces`.

## Per finire 
È richiesto un singolo reboot per rimuovere systemd come pid1.

`root@devuan~# reboot`

Ora potete tranquillamente rimuovere systemd e D-bus.

`root@devuan:~# apt-get purge systemd systemd-shim libsystemd0 dbus`

L'ambiente grafico gnome non è al momento disponibile senza systemd. Possiamo servirci di un'espressione regolare per scovare tutti i pacchetti relativi a gnome e non doverli così rimuovere manualmente uno ad uno.

`root@devuan~# apt-get purge .*gnome.*`

Potrebbe rivelarsi necessario proteggere il pacchetto xorg dalla rimozione.

`root@devuan~# apt-get install xorg`

Potreste voler sgrassare il sistema dai pacchetti raccomandati e "orfanizzati" ancora una volta.

`root@devuan:~# apt-get autoremove --purge`

Questo è anche un buon momento per rimuovere gli archivi dei vecchi pacchetti della precedente installazione di Debian.

`root@devuan:~# apt-get autoclean`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
