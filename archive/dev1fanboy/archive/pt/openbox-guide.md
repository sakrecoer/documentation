# Guia do openbox
Esta é uma introdução ao openbox para aqueles que nunca o utilizaram. Ele fornece muitos dos recursos que ambientes gráficos mais completos geralmente têm, porém se mantendo o mais minimalista possível.

## Instalando os pacotes
Vamos começar instalando uma coleção básica de pacotes para usar e configurar o openbox.

`root@devuan:~# apt-get install openbox obconf obmenu`

É útil também instalar o menu, já que habilita itens para os programas que os suportam.

`root@devuan:~# apt-get install menu`

Agora devemos instalar o gerenciador de composição 'compton'.

`root@devuan:~# apt-get install compton`

O programa idesk é usado para gerenciar ícones da área de trabalho e papéis de parede.

`root@devuan:~# apt-get install idesk`

Para uma barra de tarefas, utilizaremos o leve e customizável 'tint2'.

`root@devuan:~# apt-get install tint2`

O gerenciador de arquivos Xfe é capaz de montar volumes sem depender de algum serviço em backend, desde que tenha configurado isso no fstab.

`root@devuan:~# apt-get install xfe`

Por padrão, openbox utiliza o scrot para capturar imagem da tela quando a tecla `Print Screen` é pressionada. Basta instalar o scrot para que isso funcione.

`root@devuan:~# apt-get install scrot`

Na seção de configuração do idesk nós usaremos a imagem de fundo padrão do Devuan, conhecida como purpy, que é instalada pelo pacote desktop-base.

`root@devuan:~# apt-get install desktop-base`

## Configure o openbox
Quando iniciamos o openbox queremos ter certeza de que o gerenciador de composição, gerenciador de área de trabalho e a barra de tarefas estão disponíveis. Precisamos ter certeza de que a pasta do openbox existe em ~/.config, e então criar um arquivo de inicialização automática para que o openbox possa carregá-lo.

`user@devuan:~$ mkdir -p ~/.config/openbox`

`user@devuan:~$ editor ~/.config/openbox/autostart`

No arquivo de inicialização automática, adicione o seguinte, ciente de que cada linha termina com um `&`, exceto a última.

~~~
compton &
idesk &
tint2
~~~

## Configure o idesk
Antes de poder usar o idesk precisamos criar a pasta que ele usa para inserir ícones da área de trabalho.

`user@devuan:~$ mkdir ~/.idesktop`

Inicie o idesk para que gere a configuração padrão. Você deve matar o processo usando `ctrl + c` logo em seguida.

`user@devuan:~$ idesk`

Modifique o arquivo de configuração para que possamos trocar o papel de parede.

`user@devuan:~$ editor ~/.ideskrc`

Como isso é um teste, usaremos a imagem de fundo 'purpy' conforme mencionado anteriormente.

~~~
Background.File: /usr/share/images/desktop-base/your-way_purpy-wide-large.png
~~~

Depois configuraremos um ícone para o Xfe usando um item padrão como modelo.

`user@devuan:~$ cp ~/.idesktop/default.lnk ~/.idesktop/xfe.lnk`

`user@devuan:~$ editor ~/.idesktop/xfe.lnk`

Modifique o novo item da área de trabalho para que esteja conforme pode ver abaixo. Lembre-se de modificar a posição vertical (`Y:`) do ícone para que não fique por cima do item padrão.

~~~
table Icon
  Caption: Xfe
  Command: /usr/bin/xfe
  Icon: /usr/share/pixmaps/xfe.xpm
  Width: 48
  Height: 48
  X: 30
  Y: 120
end
~~~

## Usando o gerenciador de sessão do openbox
Se você usa um gerenciador de exibição, poderá agora fazer login ao openbox selecionando openbox-session como gerenciador de sessão. Se não, poderá usar o arquivo de configuração xinitrc para invocar o script startx depois que fizer login no console.

`user@devuan:~$ echo "exec openbox-session" > ~/.xinitrc`

`user@devuan:~$ startx`

Se preferir, poderá mudar o gerenciador de sessão padrão permanentemente para o openbox-session, para todos os usuários.

`root@devuan:~# update-alternatives --config x-session-manager`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
