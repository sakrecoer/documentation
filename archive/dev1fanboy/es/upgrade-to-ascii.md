Este documento muestra cómo actualizar desde Devuan Jessie a Devuan ASCII. Asume que se parte de una instalación de Devuan Jessie en correcto funcionamiento, y no debe utilizarse para migraciones de sistemas.

# Actualización desde Devuan Jessie a ASCII
Primero editar el archivo sources.list y cambiar la rama a ASCII.

`root@devuan:~# editor /etc/apt/sources.list`

Agregar el último *mirror* de Devuan y la rama ASCII tal como se demuestra a continuación. Comentar el resto de las líneas.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Actualizar el *keyring* de Devuan para asegurarse de tener la última versión.

`root@devuan:~# apt-get upgrade devuan-keyring`

Luego actualizar el índice de paquetes.

`root@devuan:~# apt-get update`

Finalmente actualizar el sistema.

`root@devuan:~# apt-get dist-upgrade`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>
