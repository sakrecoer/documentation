Este documento provee instrucciones genéricas para configurar la red cuando se migra desde Debian a Devuan.

# Configuración de red

Cuando se migra a Devuan es posible elegir entre wicd o configurar la red de forma manual.

La configuración manual de red tiene la ventaja de no depender de D-Bus y no requerir Network Manager. Para migraciones remotas se debe utilizar siempre una configuración de red manual o muy probablemente se perderá el acceso durante la migración.

Usar un gestor de red tiene la ventaja de su facilidad de uso y tener una interfaz GUI. Este método debe utilizarse **sólo** para instrucciones de migración que no incluyen la instalación de wicd.

## Uso del gestor de red wicd

Primero es necesario instalar el paquete wicd. El paquete completo se utiliza en este ejemplo para proveer la interfaz gtk junto con la interfaz curses para la consola.

`root@debian:~# apt-get install wicd`

El demonio network-manager debe detenerse y ser deshabilitado durante el inicio.

`root@debian:~# service network-manager stop`  
`root@debian:~# update-rc.d -f network-manager remove`

Reiniciar el demonio wicd para que se active la gestión de redes.

`root@debian:~# service wicd restart`

Los usuarios de interfaces inalámbricas deben configurar su red desde el cliente gtk en el entorno de escritorio.

## Configuración manual de redes cableadas

En versiones recientes de Debian se utilizan nombres de interfaces contraintuitivos, tales como `ens3`. Por lo tanto, por seguridad, es necesario agregar líneas de configuración tanto para el nombre de interfaz actual como para el nombre de dispositivo intuitivo.

Cabe destacar que luego de la migración los nombres de dispositivo estilo Debian ya no estarán presentes, con lo cual simplemente no serán configurados.

Las conexiones cableadas pueden ser gestionadas a través del archivo de configuración `interfaces`.

`root@debian:~# editor /etc/network/interfaces`

### Configuración automática de red

Este método configura una interfaz de red de manera automática cada vez que se detecta enlace. Modificar esta configuración de acuerdo al nombre de la interfaz si es necesario.

~~~
allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug ens3
iface ens3 inet dhcp
~~~

### Configuración estática de red
En caso de que se requiera configurar la interfaz de manera estática, ajustar las siguientes líneas de acuerdo al nombre de la interfaz y la red.

~~~
auto eth0
iface eth0 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1

auto ens3
iface ens3 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

## Conexiones inalámbricas
La configuración es similar a las conexiones cableadas, a excepción de que se requiere proveer los detalles de autenticación. Si se necesita configurar la red automáticamente, utilizar el gestor de redes wicd.

Editar el archivo interfaces para configurar la red inalámbrica.

`root@debian:~# editor /etc/network/interfaces`

Ajustar las variables adecuadamente, a fin de adaptarlas a la configuración de la interfaz y la red.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

