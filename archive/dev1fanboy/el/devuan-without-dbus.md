# Devuan δίχως D-Bus
Αυτό το κείμενο περιγράφει πως να αφαιρέσετε το D-Bus από τη Devuan,
περιλαμβάνει αλλαγή σε έναν ελαφρή διαχειριστή παραθύρων, την επιλογή
ενός φυλλομετρητή διαδυκτίου και μια εναλλακτική λύση για την αυτόματη
προσάρτηση δίσκων χωρίς το D-Bus. 

## Επιλογή διαχειριστή παραθύρων
Σε αντιδιαστολή με τα περιβάλλοντα εργασίας, οι περισσότεροι
διαχειριστές παραθύρων δεν εξαρτούνται από το D-Bus. Επομένως θα
πρέπει να επιλέξετε την εγκατατάσταση ενός διαχειριστή παραθύρων.

* blackbox
* fluxbox
* fvwm
* fvwm-crystal
* openbox

### Εγκατάσταση και ρύθμιση Fluxbox
Θα χρησιμοποιείτε τον Fluxbox καθώς είναι απλός και εύκολα κατανοητός.

`root@devuan:~# apt-get install fluxbox`

Κάντε τον Fluxbox τον προεπιλεγμένο διαχειριστή παραθύρων για τον
χρήστη σας όταν γίνεται χρήση του σεναρίου startx.

`user@devuan:~@ echo "exec fluxbox" > .xinitrc`

Μπορείτε τώρα να καλέσετε το σενάριο startx για να χρησιμοποιήσετε τον
Fluxbox.

`user@devuan:~@ startx`

Μια καλή επιλογή για τον διαχειριστή οθόνης είναι ο WDM.

`root@devuan:~# apt-get install wdm`

## Επιλογή φυλλομετρητή διαδυκτίου
Υπάρχουν μερικοί φυλλομετρητές διαδυκτίου που δεν εξαρτώνται από
συστατικά dbus, αλλά κάποιοι είναι καλύτεροι από άλλους. Εδώ είναι
μερικοί για να επιλέξετε.

* xombrero
* lynx
* links2
* dillo
* midori
* firefox-esr

Θα διαλέξουμε το πασίγνωστο Firefox ESR καθώς έχει τα ποιο πλούσια χαρακτηριστικά.

`root@devuan:~# apt-get install firefox-esr`

## Αφαίρεση του D-Bus από τη Devuan
Μπορείτε τώρα να αφαιρέσετε το dbus από τη Devuan.

`root@devuan:~# apt-get purge dbus`

Θα πρέπει επιπλέον να αφαιρέσουμε όσα πακέτα ορφάνεψαν από την
αφαίρεση του dbus.

`root@devuan:~# apt-get autoremove --purge`

## Μια απλή εναλλακτική λύση για την αυτόματη προσάρτηση
Χωρίς το D-Bus δε θα έχετε την αυτόματη προσάρτηση διαθέσιμη για τους
περισσότερους διαχειριστές αρχείων, επειδή αυτοί απαιτούν το D-Bus και
απλούστερες μέθοδοι προσάρτησης δεν υλοποιούνται πάντα. Θα θέσουμε
σημεία προσάρτησης μόνοι μας, έτσι οι ποιο λογικοί διαχειριστές να
μπορούν να προσαρτούν τόμους με μόνο μερικά κλικ.

### Χειροκίνητα σημεία προσάρτησης
Φτιάξτε ένα κατάλογο για το νέο σημείο προσάρτησης.

`root@devuan:~# mkdir /media/usb0`

Δημιουργήστε ένα αντίγραφο ασφάλειας του fstab πριν προχωρήσετε.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Τώρα μπορείτε να επεξεργαστείτε το fstab.

`root@devuan:~# editor /etc/fstab`

Χρειάζεται να προσθέσουμε ένα σημείο προσάρτησης για ένα δίσκο USB
στο τέλος του fstab. Βεβαιωθείτε πως έχετε θέσει την επιλογή `user`
έτσι ώστε απλοί χρήστες, μη διαχειριστές, να μπορούν να προσαρτήσουν
το δίσκο.

~~~
/dev/sdb1       /media/usb0     auto    user,noauto     0 0
~~~

Οι κόμβοι συσκευής για τους δίσκους usb ποικίλλουν ανά
εγκατάσταση. Μπορείτε να βρείτε ποιοι κόμβοι συσκευών θα
χρησιμοποιηθούν με τη σύνδεση της συσκευής και τη χρήση του εργαλείου
`lsblk`. 

Συνδέστε ένα δίσκο USB για να δοκιμάσετε το έργο σας.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### Επιλογή διαχειριστή αρχείων
Για έναν γραφικό διαχειριστή αρχείων που μπορεί να (από)προσαρτεί
δίσκους με βάση το fstab σας, μπορεί να χρησιμοποιήσετε το Xfe.

`root@devuan:~# apt-get install xfe`

Ένας ενδιαφέρον και μινιμαλιστικός διαχειριστής αρχείων είναι ο
Midnight Commander βασισμένος σε ncurses.

`root@devuan:~# apt-get install mc`

---

<sub>**Αυτό το έργο διατίθεται υπό την άδεια χρήσης Creative Commons Αναφορά Δημιουργού - Παρόμοια Διανομή 4.0 Διεθνές [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.el)]. Όλα τα εμπορικά σήματα αποτελούν ιδιοκτησία των αντίστοιχων ιδιοκτητών τους. Αυτό το έργο παρέχεται "ΩΣ ΕΧΕΙ" και δεν προσφέρει ΚΑΜΙΑ απολύτως εγγύηση.**</sub>
