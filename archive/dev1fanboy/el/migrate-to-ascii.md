# Μετανάστευση σε Devuan ASCII
Αυτό το κείμενο περιγράφει πως να μεταναστεύσετε στη Devuan ASCII από
τη Debian Jessie ή Stretch. Για την ώρα, η μετανάστευση στην ASCII από
τη Debian Jessie ή Stretch δεν είναι βατή εάν χρησιμοποιείτε το GNOME
ή το network-manager εξ' αιτίας κάποιων κρατημένων πακέτων, και η κάθε
μία μετανάστευση είναι λίγο διαφορετική αλλά τα παρακάτω μπορούν να
παρακάμψουν τα προβλήματα και στις δύο περιπτώσεις.

## Μετανάστευση από το περιβάλλον εργασίας GNOME
Καθώς δεν υπάρχει κάποιο μεταβατικό πακέτο ακόμη, οι χρήστες του GNOME
θα πρέπει να εγκαταστήσουν τα xfce4 και slim χειροκίνητα. Επιλέξτε τον
slim ως προεπιλεγμένο διαχειριστή οθόνης όταν ερωτηθείτε.

`root@debian:~# apt-get install xfce4 slim`

Θέστε τον διαχειριστή συνεδρίας σε startxfce4 ώστε να χρησιμοποιηθεί
αργότερα το καινούργιο περιβάλλον εργασίας.

`root@debian:~# update-alternatives --config x-session-manager`

## Αντικαθιστώντανς το network-manager με το wicd
Εάν στηρίζεστε στο network-manager για πρόσβαση στο δίκτυο, θα
χρειαστεί να αλλάξετε τώρα στο wicd. Μια άλλη επιλογή είναι η
χειροκίνητη [ρύθμιση δικτύου](network-configuration.md). **Εξ' αιτίας
έλλειψης συμβατής ρύθμισης δικτύου στη Stretch, εάν μεταναστεύετε
απομακρυσμένα ΠΡΕΠΕΙ να χρησιμοποιήσετε χειροκίνητες ρυθμίσεις
δικτύου, ή ΘΑ ΧΑΣΕΤΕ επαφή με το απομακρυσμένο σύστημα. Θα χρειαστεί
να δημιουργήσετε το αρχείο /etc/network/interfaces πριν επιχειρήσετε
την αναβάθμιση.**

`root@debian:~# apt-get install wicd`

Βεβαιωθείτε πως ο network-manager έχει απομακρυνθεί από τη διαδικασία εκκίνησης.

`root@debian:~# update-rc.d -f network-manager remove`

Οι ασύρματες συνδέσεις θα πρέπει τώρα να ρυθμιστούν να συνδέονται
αυτόματα με το wicd. Σταματήστε το network-manager πριν ρυθμίσετε το
δίκτυό σας.

`root@debian:~# /etc/init.d/network-manager stop`

## Εκτελέστε τη μετανάστευση

Η Devuan χρησιμοποιεί το sysvinit εξ' ορισμού, γι' αυτό θα το
εγκαταστήσουμε τώρα.

`root@debian:~# apt-get install sysvinit-core`

Μια επανεκκίνηση απαιτείται για την αλλαγή του sysvinit σε pid1, ώστε
να μπορέσουμε να αφαιρέσουμε το systemd.

`root@debian:~# reboot`

Τώρα μπορούμε να αφαιρέσουμε το systemd χωρίς κανένα παράπονο.

`root@debian:~# apt-get purge systemd`

Επεξεργαστείτε το αρχείο sources.list και αλλάξτε στα αποθετήρια της Devuan.

`root@debian:~# editor /etc/apt/sources.list`

Προσθέστε τους καθρέπτες της Devuan με το όνομα κλάδου
ASCII. Απενεργοποιήστε σχολιάζοντας όποιες άλλες γραμμές.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Ανανεώστε τους δείκτες των πακέτων ώστε να εγκαταστήσουμε το κλειδί
του αποθετηρίου της Devuan.

`root@debian:~# apt-get update`

Εγκαταστήστε το κλειδί της Devuan ώστε τα πακέτα να μπορούν να
πιστοποιούνται από εδώ και στο εξής.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Ανανεώστε τους δείκτες των πακέτων και πάλι ώστε να είναι τώρα
πιστοποιημένα με το κλειδί.

`root@debian:~# apt-get update`

Τέλος μπορούμε να μεταναστεύσουμε στη Devuan.

`root@debian:~# apt-get dist-upgrade`

## Εργασίες μετά τη μετανάστευση
Συστατικά του systemd θα πρέπει τώρα να αφαιρεθούν από το σύστημα.

`root@devuan:~# apt-get purge systemd-shim`

Εάν δε χρησιμοποιείτε το D-Bus ή το xorg, μπορεί να καταφέρετε να
αφαιρέσετε το libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Καθαρίστε όποια ορφανά πακέτα έχουν ξεμείνει από τη προηγούμενη
εγκατάσταση της Debian.

`root@devuan:~# apt-get autoremove --purge`

Αυτή είναι μια καλή στιγμή να καθαρίσετε τα παλιά αποθετήρια πακέτων,
απομηνάρια του Debian συστήματός σας.

`root@devuan:~# apt-get autoclean`

---

<sub>**Αυτό το έργο διατίθεται υπό την άδεια χρήσης Creative Commons Αναφορά Δημιουργού - Παρόμοια Διανομή 4.0 Διεθνές [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.el)]. Όλα τα εμπορικά σήματα αποτελούν ιδιοκτησία των αντίστοιχων ιδιοκτητών τους. Αυτό το έργο παρέχεται "ΩΣ ΕΧΕΙ" και δεν προσφέρει ΚΑΜΙΑ απολύτως εγγύηση.**</sub>
