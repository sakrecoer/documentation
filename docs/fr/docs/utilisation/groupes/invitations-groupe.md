# Gérer les invitations dans le groupe

Pour inviter quelqu'un à votre groupe vous devez&nbsp;:

  1. cliquer sur **Mes groupes** dans le menu supérieur
  * cliquer sur le groupe souhaité
  * cliquer sur le lien **Ajouter / Supprimer…** dans l'image d'illustration
  * entrer l'identifiant unique du membre à ajouter dans le champ **Inviter un nouveau membre**
  * cliquer sur le bouton **inviter un⋅e membre**

Le ou la membre invité⋅e sera notifié⋅e si il ou elle a paramétré son compte [pour recevoir les notifications](../../utilisateur et utilisatrice/parametres-compte/#notifications-par-email)

!!! note
    Par défaut les comptes invités ont le rôle de **membre**.

![](../../images/group-invitations-list-from-group-FR.png)

## Être invité⋅e à rejoindre un groupe

Quand quelqu'un vous invite à rejoindre un groupe (voir ci-dessus), vous revenez un mail de notification (si votre compte est [paramétré pour](../../utilisateur et utilisatrice/parametres-compte/#notifications-par-email)).

Depuis la page **Mes groupes** vous pouvez soit **accepter** soit **décliner** une invitation.

!!! note
    Par défaut votre rôle **membre**.

![groupe invitation](../../images/group-accept-invitation-FR.png)
