# Logging

Mobilizon outputs to the standard output. If you use `systemd` as a service manager for Mobilizon, then the logs are probably accessible through `journald`.

## Sentry

Mobilizon offers Sentry integration to track errors. Simply add the following configuration to start sending errors and stacktraces to Sentry. Restart Mobilizon afterwards.

```elixir
config :sentry,
    dsn: "https://public_key@app.getsentry.com/1", # Your Sentry DSN
    release: to_string(Application.spec(:mobilizon, :vsn))

config :logger, :console, backends: [:console, Sentry.LoggerBackend]

config :mobilizon, Mobilizon.Service.ErrorReporting,
    adapter: Mobilizon.Service.ErrorReporting.Sentry
```